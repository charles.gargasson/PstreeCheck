import volatility.plugins.common as common
import volatility.utils as utils
import volatility.win32 as win32
import json


def depth(d, level=1): 
    """ Calcul profondeur dictionnaire """
    if not isinstance(d, dict) or not d:
        return level
    return max(depth(d[k], level + 1) for k in d)

class check_pstree(common.AbstractWindowsCommand):
  """ Verification de l'ordre des processus """

  def __init__(self, config, *args, **kwargs):
      common.AbstractWindowsCommand.__init__(self, config, *args, **kwargs)
      config.add_option('config', default = "config.txt", help = 'Configuration file', type = 'str')

  def config(self):
      """ Chargement de la configuration """
      f = open(self._config.config, "r")
      config = eval(f.read())

      return config

  def pslist(self):
      """ Fonction de recuperation de la liste des process """

      addr_space = utils.load_as(self._config)

      """ Appel pslist, tasks est un objet de type generateur """
      tasks = win32.tasks.pslist(addr_space)

      """ Extraction de la liste des process vers un dictionnaire """
      tasks_dict = {}
      for task in tasks:
          tasks_dict[int(task.UniqueProcessId)] = {
            'TIME' : str(task.CreateTime),
            'NAME' : str(task.ImageFileName),
            'PPID' : int(task.InheritedFromUniqueProcessId)
          }

      return tasks_dict

  def depth(d, level=1):
      """ Calcul profondeur dictionnaire """
      if not isinstance(d, dict) or not d:
          return level
      return max(depth(d[k], level + 1) for k in d)

  def check(self, config, pslist):
      """ Recherche d'anomalies """

      """ Pour chaque pid dans la liste des process """
      result = {}
      for pid, attr in pslist.iteritems():

          """ Est-il dans la conf ? """
          if attr['NAME'] in config:
              ps_config = config[attr['NAME']]
          else:
              continue

          """ Nom deja vu ? """
          if attr['NAME'] not in result:
              result[attr['NAME']] = {}

          """ Creation entree pid """
          result[attr['NAME']][pid] = {}

          """ Unique ? """
          if len(result[attr['NAME']]) != 1 and 'UNIQUE' in ps_config and ps_config['UNIQUE'] == 1:
              newitem = { 'UNIQUE' : 'FALSE' }
              for key,_ in result[attr['NAME']].iteritems():
                  result[attr['NAME']][key].update(newitem)

          """ ID parent ? """
          if 'PPID' in ps_config and attr['PPID'] != ps_config['PPID']:
              newitem = { 'PPID' : { 'EXPECTED' : ps_config['PPID'], 'FOUND' : attr['PPID'] } }
              result[attr['NAME']][pid].update(newitem)

          """ Nom parent ? """
          if 'PNAME' in ps_config and pslist[attr['PPID']]['NAME'] != ps_config['PNAME']:
              newitem = { 'PNAME' : { 'EXPECTED' : ps_config['PNAME'], 'FOUND' : pslist[attr['PPID']]['NAME'] } }
              result[attr['NAME']][pid].update(newitem)

          """ Le process a-t-il le bon ID ? """
          if 'PID' in ps_config and pid != ps_config['PID']:
              newitem = { 'PID' : { 'EXPECTED' : ps_config['PID'], 'FOUND' : pid } }
              result[attr['NAME']][pid].update(newitem)

      for k in result.keys():
          if depth(result[k]) < 3 :
             del result[k]

      return result

  def calculate(self):
      """ Main """
      config = self.config()
      pslist = self.pslist()
      check = self.check(config, pslist)
      return check

  def render_text(self, outfd, data):
      """ Affichage """
      outfd.write("\n{0}\n\n".format(json.dumps(data,sort_keys=True, indent=3)))


