# PstreeCheck

Ex dump ??
```
./td_volatility/ram3/ram3/dump.vmem
./td_volatility/ram4/ram4/dump.vmem
./td_volatility/ram1/ram1/dump.vmem
./td_volatility/ram2/ram2/dump.vmem
```


python /usr/share/volatility/vol.py --plugins="/git/perso/PstreeCheck/" -f dump.vmem check_pstree
python /usr/share/volatility/vol.py --plugins="/home/chga/Bureau/AFTI/FORENSIC/PstreeCheck" -f /home/chga/Bureau/AFTI/FORENSIC/td_volatility/ram1/ram1/dump.vmem check_pstree --config config.txt



```
python /usr/share/volatility/vol.py --plugins="/git/perso/PstreeCheck/" -f /home/chga/Documents/FORENSIC/td_volatility/ram3/ram3/dump.vmem check_pstree
Volatility Foundation Volatility Framework 2.6

{
   "lsass.exe": {
      "680": {
         "UNIQUE": "FALSE"
      }, 
      "868": {
         "PNAME": {
            "EXPECTED": "winlogon.exe", 
            "FOUND": "services.exe"
         }, 
         "UNIQUE": "FALSE"
      }, 
      "1928": {
         "PNAME": {
            "EXPECTED": "winlogon.exe", 
            "FOUND": "services.exe"
         }, 
         "UNIQUE": "FALSE"
      }
   }
}
```


```
vol -f ram1/ram1/dump.vmem  pstree
Volatility Foundation Volatility Framework 2.6
Name                                                  Pid   PPid   Thds   Hnds Time
-------------------------------------------------- ------ ------ ------ ------ ----
 0x810b1660:System                                      4      0     58    190 1970-01-01 00:00:00 UTC+0000
. 0xff2ab020:smss.exe                                 544      4      3     21 2010-08-11 06:06:21 UTC+0000
.. 0xff1ec978:winlogon.exe                            632    544     19    513 2010-08-11 06:06:23 UTC+0000
... 0xff255020:lsass.exe                              688    632     21    349 2010-08-11 06:06:24 UTC+0000
... 0xff247020:services.exe                           676    632     16    269 2010-08-11 06:06:24 UTC+0000
.... 0xff1b8b28:vmtoolsd.exe                         1668    676      5    221 2010-08-11 06:06:35 UTC+0000
..... 0x80f167b8:cmd.exe                             1368   1668      0 ------ 2010-08-15 17:43:45 UTC+0000
.... 0x80ff88d8:svchost.exe                           856    676     18    203 2010-08-11 06:06:24 UTC+0000
.... 0xff1d7da0:spoolsv.exe                          1432    676     13    135 2010-08-11 06:06:26 UTC+0000
.... 0x80fbf910:svchost.exe                          1028    676     88   1426 2010-08-11 06:06:24 UTC+0000
..... 0x80f60da0:wuauclt.exe                         1732   1028      7    178 2010-08-11 06:07:44 UTC+0000
..... 0x80f94588:wuauclt.exe                          468   1028      7    139 2010-08-11 06:09:37 UTC+0000
..... 0xff364310:wscntfy.exe                          888   1028      4     32 2010-08-11 06:06:49 UTC+0000
.... 0xff217560:svchost.exe                           936    676     11    268 2010-08-11 06:06:24 UTC+0000
.... 0xff143b28:TPAutoConnSvc.e                      1968    676      5    100 2010-08-11 06:06:39 UTC+0000
..... 0xff38b5f8:TPAutoConnect.e                     1084   1968      4     66 2010-08-11 06:06:52 UTC+0000
.... 0xff22d558:svchost.exe                          1088    676      6     80 2010-08-11 06:06:25 UTC+0000
.... 0xff218230:vmacthlp.exe                          844    676      1     24 2010-08-11 06:06:24 UTC+0000
.... 0xff25a7e0:alg.exe                               216    676      7    108 2010-08-11 06:06:39 UTC+0000
.... 0xff203b80:svchost.exe                          1148    676     15    212 2010-08-11 06:06:26 UTC+0000
.... 0xff1fdc88:VMUpgradeHelper                      1788    676      5    102 2010-08-11 06:06:38 UTC+0000
.. 0xff1ecda0:csrss.exe                               608    544     11    434 2010-08-11 06:06:23 UTC+0000
 0xff3865d0:explorer.exe                             1724   1708     18    414 2010-08-11 06:09:29 UTC+0000
. 0xff22f3d0:aelas.exe                               1984   1724     19    139 2010-08-15 17:43:26 UTC+0000
. 0xff374980:VMwareUser.exe                           452   1724     11    208 2010-08-11 06:09:32 UTC+0000
. 0xff3667e8:VMwareTray.exe                           432   1724      4     53 2010-08-11 06:09:31 UTC+0000
```



